/*
 * Created by Daniel Nazarian on 3/4/19 2:04 PM
 * Copyright (c) 2019 . All rights reserved.
 *
 * Please do NOT use, edit, distribute, or otherwise use this code without consent.
 * For questions, comments, concerns, and more -> dnaz@danielnazarian.com
 */

import 'package:flutter/material.dart';
import 'package:leonidas/models/json_news_api.dart';

/// BASE ===================================================/

Widget cardBase({
  Widget child,
  double radius = 4.0,
  double elevation = 8.0,
  Function onTap,})
{
  return InkWell(
    onTap: onTap,
    child: Card(
      child: child,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(radius))),
      elevation: elevation,
    ),
  );
}

Widget pageTitle(BuildContext context,{
  String title = ""}) {
  return Container();
}

/// DASHBOARD CARDS ===================================================/

Widget cardDashboard(BuildContext context, String title,
    {Widget child, Function onTap, Color colorBackground = Colors.white})
{
  return cardBase(
    onTap: onTap,
    elevation: 16.0,
    child: Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Material(
            elevation: 0.0,
            borderRadius: BorderRadius.all(Radius.circular(4.0)),
            color: colorBackground,
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(title,
                style: Theme.of(context).textTheme.title.copyWith(
//                  color: Colors.black,
                ),
              ),
            ),
          ),
           Container(
//             decoration: gradientColorToTransparent(context, color: colorBackground),
             child: child,
           ),
        ],
      ),
    ),
  );
}


/// NEWS CARDS ===================================================/

Widget cardArticle(Article article,{
  Function onTap, Widget child,
}) {
  return cardBase(
    onTap: onTap,
    child: Stack(
      children: <Widget>[
        ClipRect(
          child: Image.network(
            article.urlToImage,
            fit: BoxFit.cover,
          ),
        ),


        Text(article.author ?? "PENIS"),
      ],
    )
  );
}