/*
 *  Created by Daniel Nazarian on 3/3/19 4:49 PM
 *  Copyright (c) 2019 . All rights reserved.
 *
 *  For questions, comments, concerns, and more -> dnaz@danielnazarian.com
 */

import 'package:flutter/material.dart';

/// GRADIENTS ===================================================/

BoxDecoration gradientMain(BuildContext context, {bool alternate = false}) {
  return BoxDecoration(
    gradient: LinearGradient(
      begin: Alignment.topRight,
      end: Alignment.bottomLeft,
      colors: [
        !alternate ? Theme.of(context).primaryColor : Theme.of(context).primaryColorLight,
        !alternate ? Theme.of(context).primaryColorLight : Theme.of(context).primaryColor,
        !alternate ? Colors.white : Theme.of(context).primaryColorDark,
      ],
    ),
  );
}


BoxDecoration gradientColorToTransparent(BuildContext context,
    {Color color = Colors.white, Alignment begin = Alignment.centerLeft, Alignment end = Alignment.centerRight})
{
  return BoxDecoration(
    gradient: LinearGradient(
      begin: begin,
      end: end,
      colors: [
        color,
        color,
        Colors.transparent,
      ],
    ),
  );
}