
/*
 * Created by Daniel Nazarian on 3/4/19 2:04 PM
 * Copyright (c) 2019 . All rights reserved.
 *
 * Please do NOT use, edit, distribute, or otherwise use this code without consent.
 * For questions, comments, concerns, and more -> dnaz@danielnazarian.com
 */

import 'package:flutter/material.dart';
import 'package:leonidas/models/user.dart';

/// ROUNDED =========================================================================/

Widget imageRounded(Image image, {double radius = 8.0}) {
  return ClipRRect(
    borderRadius: new BorderRadius.circular(radius),
    child: image
  );
}