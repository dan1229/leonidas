/*
 * Created by Daniel Nazarian on 3/4/19 2:04 PM
 * Copyright (c) 2019 . All rights reserved.
 *
 * Please do NOT use, edit, distribute, or otherwise use this code without consent.
 * For questions, comments, concerns, and more -> dnaz@danielnazarian.com
 */

import 'dart:convert';

import 'package:flutter/services.dart';
import 'package:leonidas/services/newsapi.dart';
import 'package:leonidas/services/resource_loader.dart';

/// SERVICE MANAGER ===================================================/

class ServiceManager {

  ServiceManager({bool demo = false}) {
    resourceLoader = ResourceLoader();
    newsApi = NewsApi(resourceLoader.keys.apiKeyNewsApi);
    demo = demo;
  }

  ResourceLoader resourceLoader;
  bool demo = false;

  ///***** NEWS API *****///
  NewsApi newsApi;
  getArticlesList() => this.newsApi.articles;
  
  getApiNewsApi(String query) {
    if (demo) { // DEMO MODE
      return resourceLoader.demoNewsApi.articles;
    }
    else {
      if(newsApi.validList) {
        return newsApi.articles;
      } else {
        return newsApi.callNewsQueryApi(query);
      }
    }
  }
}

