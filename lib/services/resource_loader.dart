/*
 * Created by Daniel Nazarian on 3/4/19 2:04 PM
 * Copyright (c) 2019 . All rights reserved.
 *  
 * Please do NOT use, edit, distribute, or otherwise use this code without consent.
 * For questions, comments, concerns, and more -> dnaz@danielnazarian.com
 */


import 'dart:convert';

import 'package:flutter/services.dart';
import 'package:leonidas/models/json_news_api.dart';

/// RESOURCE LOADER =========================================================================/

class ResourceLoader<String> {

  ResourceLoader() {
    loadResourceKeys();
    getDemoData();
  }

  ///***** API KEYS *****///
  ResourceKeys keys = ResourceKeys();
  
  loadResourceKeys() {
    rootBundle.loadStructuredData<ResourceKeys>(keys.path, (jsonStr) async {
      keys = ResourceKeys.fromJson(json.decode(jsonStr));
      return this.keys;
    });
  }

  ///***** DEMOS *****///
  NewsApi demoNewsApi;
  
  getDemoData() {
    demoNewsApi = newsApiFromJson(loadResourceDemos());
  }

  loadResourceDemos() async {
    var str = await rootBundle.loadString("assets/demo/newsapi.json");
    return str;
  }

}


/// RESOURCES =========================================================================/

///***** KEYS *****///
class ResourceKeys {

  final String path = "assets/keys.json";
  String apiKeyNewsApi;

  ResourceKeys({this.apiKeyNewsApi});

  factory ResourceKeys.fromJson(Map<String, dynamic> jsonMap) {
    return ResourceKeys(
        apiKeyNewsApi: jsonMap["api_key_newsapi"]
    );
  }
}