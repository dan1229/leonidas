/*
 * Created by Daniel Nazarian on 3/4/19 2:04 PM
 * Copyright (c) 2019 . All rights reserved.
 *
 * Please do NOT use, edit, distribute, or otherwise use this code without consent.
 * For questions, comments, concerns, and more -> dnaz@danielnazarian.com
 */

import 'package:leonidas/models/json_news_api.dart';
import 'package:leonidas/services/helpers.dart';

/// NEWSAPI ===================================================/

class NewsApi {

  NewsApi(String apiKey) {
    apiKey = apiKey;
  }

  String apiKey;
  List<Article> articles = List();
  
  bool get validList => (this.articles == null || this.articles?.length == 0);

  ///***** URL BUILDERS *****///
  String urlBuilderQuery(String query, {int pageSize = 10}) {
    return "https://newsapi.org/v2/top-headlines"
      "?q=$query&category=business&pageSize=$pageSize"
      "&apiKey=$apiKey";
  }


  ///***** API CALL *****///
  Future<List<Article>> callNewsQueryApi(String query) async {
    var url = urlBuilderQuery(query);
    var response = await httpGet(url);

    if (response.statusCode == 200) { // Successful HTTP request
      final newsApi = newsApiFromJson(response.body);
      this.articles = newsApi.articles;
    }
    return this.articles;
  }

}