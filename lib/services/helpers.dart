/*
 *  Created by Daniel Nazarian on 3/3/19 1:59 AM
 *  Copyright (c) 2019 . All rights reserved.
 *
 *  For questions, comments, concerns, and more -> dnaz@danielnazarian.com
 */

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:leonidas/models/user.dart';
import 'package:leonidas/news.dart';

/// HTTP ===================================================/

httpGet(String url, {String tag = ""}) async {
  print("$tag:$url");
  return await http.get(url);
}


/// NAVIGATION =========================================================================/

void navigateToNewsPage(BuildContext context, User user) {
  Navigator.push(context,
    MaterialPageRoute(
        builder: (context) => NewsPage(user: user)
    ),
  );
}

/// DISPLAY MEASUREMENETS ===================================================/

double measureDisplayHeight(BuildContext context) {
  return MediaQuery.of(context).size.height;
}

double measureDisplayWidth(BuildContext context) {
  return MediaQuery.of(context).size.width;
}