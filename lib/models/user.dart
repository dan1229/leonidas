/*
 * Created by Daniel Nazarian on 3/4/19 2:04 PM
 * Copyright (c) 2019 . All rights reserved.
 *
 * Please do NOT use, edit, distribute, or otherwise use this code without consent.
 * For questions, comments, concerns, and more -> dnaz@danielnazarian.com
 */

import 'dart:math';

import 'package:leonidas/services/service_manager.dart';

/// USER ===================================================/

class User {

  User({bool demo = false}) {
    serviceManager = ServiceManager(demo: demo);
  }

  ServiceManager serviceManager;

  ///***** BASIC INFO *****///
  String name = "User";

  String randomInterest() => this.interests[Random().nextInt(this.interests.length)];
  List<String> interests = [
    "apple",
    "microsoft"
    "amazon",
    "netflix",
    "bitcoin",
  ];

  // portfolio
  // watchlist
  // ranking algo
}