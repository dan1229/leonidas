/*
 * Created by Daniel Nazarian on 3/4/19 2:04 PM
 * Copyright (c) 2019 . All rights reserved.
 *
 * Please do NOT use, edit, distribute, or otherwise use this code without consent.
 * For questions, comments, concerns, and more -> dnaz@danielnazarian.com
 */

import 'dart:collection';

/// STOCK LIST ===================================================/

class StockList<Stock> extends ListBase {
  List<Stock> stocks;

  StockList() : stocks = List<Stock>();

  ///***** OVERRIDES *****///
  @override
  Stock operator [](int index) {
    return stocks[index];
  }

  @override
  void operator []=(int index, value) {
    stocks[index] = value;
  }

  @override
  int get length => stocks.length;

  @override
  set length(int newLength) {
    stocks.length = newLength;
  }

}