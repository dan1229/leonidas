/*
 * Created by Daniel Nazarian on 3/4/19 2:08 PM
 * Copyright (c) 2019 . All rights reserved.
 *
 * Please do NOT use, edit, distribute, or otherwise use this code without consent.
 * For questions, comments, concerns, and more -> dnaz@danielnazarian.com
 */

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:leonidas/dashboard.dart';
import 'package:leonidas/models/user.dart';

/// DEMO MODE =========================================================================/

const bool demo = false;

/// MAIN =========================================================================/

void main() {
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
  runApp(LeonidasApp());
}

class LeonidasApp extends StatelessWidget {
  final User user = User(demo: demo);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Leonidas',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        accentColor: Colors.blueGrey[600],
        primaryColorLight: Colors.deepPurple[300],
        primaryColor: Colors.deepPurple[600],
        primaryColorDark: Colors.deepPurple[900],
        primarySwatch: Colors.deepPurple,
        brightness: Brightness.dark,
      ),
      home: Dashboard(user: user),
    );
  }
}