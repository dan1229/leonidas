/*
 * Created by Daniel Nazarian on 3/4/19 2:08 PM
 * Copyright (c) 2019 . All rights reserved.
 *
 * Please do NOT use, edit, distribute, or otherwise use this code without consent.
 * For questions, comments, concerns, and more -> dnaz@danielnazarian.com
 */

import 'dart:async';
import 'package:flutter/widgets.dart';
import 'package:meta/meta.dart';

abstract class BaseState<T extends StatefulWidget> extends State<T> {
  bool initial;

  @override
  initState() {
    super.initState();
    initial = true;
  }
  
  @override
  Widget build(BuildContext context) {
//    debugPrint("CustomState(${T.toString()}).build executed");
    return FutureBuilder (
        future: loadWidget(context, initial),
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          if (snapshot.hasData) {
            if (snapshot.data) {
              initial = false;
              return customBuild(context);
            }
          } else {
            return Center(
              child: Text("LOADING BITCH"),
            );
          }
        }
    );
  }

  @protected
  Widget customBuild(BuildContext context) {
//    debugPrint("CustomState(${T.toString()}).customBuild executed");
    return null;
  }

  @protected
  Future<bool> loadWidget(BuildContext context, bool isInit) async {
//    debugPrint("CustomState(${T.toString()}).loadWidget executed " + (isInit?"for the first time":"again"));
    return true;
  }

}