/*
 * Created by Daniel Nazarian on 3/4/19 2:08 PM
 * Copyright (c) 2019 . All rights reserved.
 *
 * Please do NOT use, edit, distribute, or otherwise use this code without consent.
 * For questions, comments, concerns, and more -> dnaz@danielnazarian.com
 */

import 'package:flutter/material.dart';
import 'package:leonidas/models/user.dart';
import 'package:leonidas/services/helpers.dart';
import 'package:leonidas/widgets/cards.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';


/// DASHBOARD ===================================================/

class Dashboard extends StatefulWidget {
  final User user;

  const Dashboard({Key key, this.user}) : super(key: key);

@override
_DashboardState createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {
  var heightGridTile = 200.0;

  ///***** INIT *****///
  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    heightGridTile = measureDisplayHeight(context) * 0.2;
  }

  ///***** BUILD *****///
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Dashboard"),
        leading: GestureDetector(
          child: Icon(Icons.menu),
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: StaggeredGridView.count(
          crossAxisCount: 2,
          crossAxisSpacing: 8.0,
          mainAxisSpacing: 8.0,
          staggeredTiles: [
            StaggeredTile.extent(2, heightGridTile),
            StaggeredTile.extent(1, heightGridTile),
            StaggeredTile.extent(1, heightGridTile),
            StaggeredTile.extent(2, heightGridTile),
            StaggeredTile.extent(2, heightGridTile),
          ],
          children: <Widget> [
            cardDashboard(context,
              "Top Picks",
              colorBackground: Theme.of(context).primaryColor,
              child: Row
                (
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text("Comments..."),
//                  Material()
                ],
              ),
            ),
            cardDashboard(context,
              "News",
              colorBackground: Colors.green,
              onTap: () {
                navigateToNewsPage(context, widget.user);
              },
              child: Column
                (
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget> [
                    Text("news..."),
                  ]
              ),
            ),
            cardDashboard(context,
              "Stats",
              colorBackground: Colors.orangeAccent,
              child: Column
                (
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget> [
                    Text("Stats..."),
                  ]
              ),
            ),
            cardDashboard(context,
              "Portfolio",
              colorBackground: Colors.blueAccent,
              child: Column
                (
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget> [
                  Text("Portfolio!")
                ],
              ),
            ),
            cardDashboard(context,
              "Planner",
              colorBackground: Colors.redAccent,
              child: Text("Planner..."),
            )
          ],
        ),
      ),
    );
  }
}
