/*
 * Created by Daniel Nazarian on 3/4/19 2:08 PM
 * Copyright (c) 2019 . All rights reserved.
 *
 * Please do NOT use, edit, distribute, or otherwise use this code without consent.
 * For questions, comments, concerns, and more -> dnaz@danielnazarian.com
 */

import 'package:flutter/material.dart';
import 'package:leonidas/base_state.dart';
import 'package:leonidas/models/json_news_api.dart';
import 'package:leonidas/models/user.dart';
import 'package:leonidas/widgets/cards.dart';


/// NEWS PAGE =========================================================================/

class NewsPage extends StatefulWidget {
  final User user;

  const NewsPage({Key key, this.user}) : super(key: key);

  @override
  _NewsPageState createState() => _NewsPageState();
}

class _NewsPageState extends BaseState<NewsPage> {
  List<Article> articles;

  @override
  Widget customBuild(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Portfolio News"),
      ),
      body: articles.length == 0 ?
      Center(
        child: Text("Nothing available right now :/",
          style: Theme.of(context).textTheme.display1,
        ),
      )
      :
      ListView.builder(
        itemCount: articles.length,
        itemBuilder: (context, index) {
          return cardArticle(widget.user.serviceManager.getArticlesList()[index]);
        }
      ),
    );
  }

  @override
  Future<bool> loadWidget(BuildContext context, bool isInit) async {
    articles = await widget.user.serviceManager.newsApi.callNewsQueryApi(widget.user.randomInterest());
    return super.loadWidget(context, isInit);
  }
}
